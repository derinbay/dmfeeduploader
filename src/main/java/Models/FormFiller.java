package Models;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static Models.FormActionType.*;

/**
 * Created by taylanderinbay on 8.11.15.
 */
public class FormFiller {
    private final List<FormAction> actions = new ArrayList<>();

    public static FormFiller by() {
        return new FormFiller();
    }

    public FormFiller dec(String inputPropertyName, double amount) {
        actions.add(new FormAction(inputPropertyName, "" + amount, DEC));
        return this;
    }

    public FormFiller inc(String inputPropertyName, double amount) {
        actions.add(new FormAction(inputPropertyName, "" + amount, INC));
        return this;
    }

    public FormFiller click(String inputPropertyName) {
        actions.add(new FormAction(inputPropertyName, null, CLICK));
        return this;
    }

    public FormFiller hover(String inputPropertyName) {
        actions.add(new FormAction(inputPropertyName, null, CLICK));
        return this;
    }

    public FormFiller type(String inputPropertyName, String text) {
        actions.add(new FormAction(inputPropertyName, text, TYPE));
        return this;
    }

    public FormFiller typeAndSelect(String inputPropertyName, String text, Consumer<Browser> consumer) {
        actions.add(new FormAction(inputPropertyName, text, TYPEANDSELECT, consumer));
        return this;
    }

    public FormFiller value(String inputPropertyName, String text) {
        actions.add(new FormAction(inputPropertyName, text, VALUE));
        return this;
    }

    public FormFiller select(String selectPropertyName, String option) {
        actions.add(new FormAction(selectPropertyName, option, SELECT));
        return this;
    }

    public FormFiller standartSelect(String selectPropertyName, String option) {
        actions.add(new FormAction(selectPropertyName, option, STANDARTSELECT));
        return this;
    }

    public FormFiller pickDate(String datePropertyName) {
        actions.add(new FormAction(datePropertyName, null, PICK_DATE));
        return this;
    }

    public FormFiller pickDateAfter(String datePropertyName, String day) {
        actions.add(new FormAction(datePropertyName, day, PICK_DATE_AFTER));
        return this;
    }

    public void fill(WebComponent webComponent) {
        Browser browser = webComponent.browser();

        browser.loadElements(webComponent);

        for (FormAction action : actions) {
            action.exec(webComponent);
        }
    }
}
