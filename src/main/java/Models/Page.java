package Models;

import Users.Visitor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by taylanderinbay on 7.11.15.
 */
public abstract class Page<V extends Visitor> extends WebComponent {
    private static final Logger logger = LogManager.getLogger(Page.class);

    protected String url;

    private Popup popup;

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }

    public boolean login() {
        throw new IllegalStateException("Page doesn't support login activity");
    }

    public boolean logout() {
        throw new IllegalStateException("Page doesn't support logout activity");
    }

    public String url() {
        return this.url;
    }

    public String currentUrl() {
        return this.browser.currentURL();
    }

    public Popup currentPopup() {
        return this.popup;
    }

    public void reset() {
        throw new IllegalStateException("Page doesn't have any form to reset");
    }

    public Popup openAddPopup() {
        throw new IllegalStateException("Page doesn't support new record popup");
    }

    public void forgot(String what) {
        throw new IllegalStateException("Page doesn't support forgot " + what);
    }

    public void refresh() {
        browser.refresh();
    }
}
