package Models;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by taylanderinbay on 8.11.15.
 */
public abstract class Popup extends WebComponent {
    private static final Logger logger = LogManager.getLogger(Popup.class);

    protected WebComponent opener;
}

