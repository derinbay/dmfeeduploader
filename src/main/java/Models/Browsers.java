package Models;

/**
 * Created by taylanderinbay on 8.11.15.
 */
class Browsers {

    private static String defaultBrowserName() {
        return Config.defaultBrowserName;
    }

    private static Browser run(BrowserType browserType) {
        Browser browser = null;

        switch (browserType) {
            case CHROME:
                browser = new Chrome();
                break;
        }

        return browser;
    }

    public static Browser runDefault() {
        return run(BrowserType.byName(defaultBrowserName()));
    }
}

