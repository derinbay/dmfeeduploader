package Models;

import javax.annotation.concurrent.Immutable;
import java.util.function.Consumer;

/**
 * Created by taylanderinbay on 8.11.15.
 */
@Immutable
class FormAction {
    private final FormActionType type;
    private final String propertyName;
    private final String value;
    private final Consumer<Browser>[] consumers;

    @SafeVarargs
    public FormAction(String propertyName, String value, FormActionType type, Consumer<Browser>... consumers) {
        this.propertyName = propertyName;
        this.value = value;
        this.type = type;
        this.consumers = consumers;
    }

    public void exec(WebComponent webComponent) {
        type.exec(webComponent, propertyName, value, consumers);
    }
}
