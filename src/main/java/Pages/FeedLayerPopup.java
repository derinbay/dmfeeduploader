package Pages;

import Models.Popup;
import Models.WebComponent;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by taylanderinbay on 8.11.15.
 */
public class FeedLayerPopup extends Popup {

    @FindBy(xpath = "(//*[@class='_58al'])[1]")
    private WebElement feedName;

    @FindBy(xpath = "(//label[contains(@class, 'uiInputLabelInput')][1]/../..//ul[contains(@class,'uiList')])[2]//input")
    private WebElement url;

    @FindBy(xpath = "//label[text()='6']/../input")
    private WebElement hour;

    @FindBy(xpath = "//label[text()='00']/../input")
    private WebElement minute;

    @FindBy(xpath = "//label[text()='PM']/../input")
    private WebElement timeFormat;

    @FindBy(xpath = "//div[contains(@class, 'uiLayer')]//span[text()='Monthly']/../..")
    private WebElement period;

    @FindBy(xpath = "//div[contains(@class, 'uiLayer')]//div[contains(@class,'uiContextualLayer')]//span[text()='Daily']/../..")
    private WebElement dailyPeriod;

    @FindBy(xpath = "//span[text()='Schedule Upload']/..")
    private WebElement uploadButton;

    public FeedLayerPopup(WebComponent component) {
        this.browser = component.browser();
        this.xpath = "//div[@class='_4t2a']";
        this.opener = component;
    }

    public void addSchedule(String[] feeds) {
        browser.loadElements(this);
        feedName.sendKeys(feeds[0]);
        url.sendKeys(feeds[1]);
        timeFormat.click();
        timeFormat.sendKeys("a");
        hour.click();
        hour.sendKeys("10");
//        period.click();
//        dailyPeriod.click();
        uploadButton.click();
        browser.waitForAjax();
    }
}
