package Pages;

import Models.Page;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static Models.FormFiller.by;


/**
 * Created by taylanderinbay on 7.11.15.
 */
public class FacebookBusinessPage extends Page {

    private static final String PAGE_URL = "https://business.facebook.com/products/catalog?business_id=774132972653171&catalog_id=441525076003089";
    private static final Logger logger = LogManager.getLogger(FacebookBusinessPage.class);

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "pass")
    private WebElement password;

    @FindBy(xpath = "//*[@id='loginbutton']/input")
    private WebElement loginButton;

    public FacebookBusinessPage() {
        this.url = PAGE_URL;
    }

    FeedLayerPopup getFeedLayer() {
        browser.findElement(By.xpath("//span[text()='Add Product Feed']/..")).click();
        FeedLayerPopup layerPopup = new FeedLayerPopup(this);
        browser.waitForAjax();

        return layerPopup;
    }

    public void login(String facebookEmail, String facebookPassword) {
        by().type("email", facebookEmail)
                .type("password", facebookPassword)
                .click("loginButton").fill(this);

        browser.waitForPresenceOf(30, By.xpath("//*[@id='bizsitePageContainer']//*[text()='N11 Product Catalog']"));
    }

    public void clickToButton(String button) {
        browser.findElement(By.xpath("//*[text()='" + button + "']/..")).click();
        browser.waitForAjax();
    }

    public void createScheduleUpload(String csvFilePath) {
        BufferedReader br;
        String[] feeds;
        String line;
        String csvSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(csvFilePath));
            while ((line = br.readLine()) != null) {
                FeedLayerPopup layerPopup = getFeedLayer();
                feeds = line.split(csvSplitBy);
                layerPopup.addSchedule(feeds);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
