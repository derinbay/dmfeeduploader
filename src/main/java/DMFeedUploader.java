import Pages.FacebookBusinessPage;
import Users.Visitor;

/**
 * Created by taylanderinbay on 7.11.15.
 */
class DMFeedUploader {

    public static void main(String args[]) throws Exception {
        System.setProperty("selenium_env", "qa");
        System.setProperty("browser", "chrome");

        /*Alttaki 3 satira facebook login bilgileri ve csv dosyasinin bilgisayardaki yolu girilecek*/
        String csvFile= "/Users/taylanderinbay/Downloads/fb.csv";
        String facebookEmail = "facebook email";
        String facebookPassword = "facebook password";


        FacebookBusinessPage facebookBusinessPage = new FacebookBusinessPage();
        Visitor visitor = Visitor.aVisitor().open(facebookBusinessPage);
        facebookBusinessPage.login(facebookEmail, facebookPassword);
        facebookBusinessPage.clickToButton("Product Feeds");

        facebookBusinessPage.createScheduleUpload(csvFile);

        visitor.closeBrowser();
    }
}
