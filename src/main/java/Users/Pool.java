package Users;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Stack;

/**
 * Created by taylanderinbay on 8.11.15.
 */
abstract class Pool<V extends Account> {
    private static final Logger logger = LogManager.getLogger(VisitorPool.class);

    private final Stack<V> visitors = new Stack<>();

    private final String poolName;

    Pool(String poolName) {
        this.poolName = poolName;
    }

    void add(V visitor) {
        visitors.add(visitor);
        visitor.setPool(this);
    }

    protected abstract void postPop(V account);

    protected abstract void preFree(V account);

    public V pop() {
        String VISITOR_LOCK = "LOCK";
        synchronized (VISITOR_LOCK) {
            V visitor = null;

            int counter = 0;
            while (true) {
                if (counter > 300) {
                    logger.error("I had tried to fetch a " + poolName + " about 5 min., but I hadn't.");
                    break;
                }

                if (!visitors.isEmpty()) {
                    visitor = visitors.pop();
                    if (visitor != null) {
                        logger.trace("A " + poolName + " {" + visitor.getUsername() + "} is popped from pool.");
                        break;
                    }
                } else {
                    logger.trace(poolName + " pool is empty. I'm waiting...");
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                counter++;
            }
            postPop(visitor);
            return visitor;
        }
    }

    public void free(V visitor) {
        preFree(visitor);
        add(visitor);
    }
}
