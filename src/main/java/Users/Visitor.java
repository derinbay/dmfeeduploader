package Users;

import Models.Browser;
import Models.Page;
import Models.Popup;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;

import java.util.Set;

import static Models.Browser.openThe;

/**
 * Created by taylanderinbay on 7.11.15.
 */
public class Visitor<V extends Visitor> implements Account<VisitorPool> {
    public Browser browser;
    private V origin;
    private VisitorPool<V> pool;
    private String id;
    private boolean online;
    private String fullname;
    private String username;
    private String password;
    private String email;
    private String reqId;
    private boolean dontSendPromo;
    private String tcNo;
    private String appKey;
    private String appSecret;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }


    private Visitor(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static Visitor aVisitor() {
        return new Visitor("", "");
    }

    public static Visitor aVisitor(String username, String password) {
        return new Visitor(username, password);
    }

    public static Visitor aVisitor(String email) {
        return new Visitor("", "").setEmail(email);
    }

    @Override
    public void setPool(VisitorPool pool) {
        this.pool = pool;
    }

    public V invalidUsername() {
        this.username = "invalid";
        return (V) this;
    }

    public V invalidPassword() {
        this.password = "invalid";
        return (V) this;
    }

    public <P extends Page> V open(P page) {
        return open(page, null);
    }

    <P extends Page> V open(P page, Set<Cookie> cookies) {
        this.browser = openThe(page, cookies).byMe(this);
        this.origin().browser = this.browser;
        return (V) this;
    }

    public <P extends Page> V goTo(P page) {
        this.browser.goTo(page);
        return (V) this;
    }

    public V changePage(Page page) {
        this.browser().changePage(page);
        return (V) this;
    }

    public V refresh() {
        this.browser.refresh();
        return (V) this;
    }

    public Visitor login() {
        online = browser.page().login();
        return this;
    }

    public Visitor logout() {
        online = !browser.page().logout();
        return this;
    }

    public boolean isOnline() {
        return online;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    public V setUsername(String username) {
        this.username = username;
        return (V) this;
    }

    public String getPassword() {
        return this.password;
    }

    public V setPassword(String password) {
        this.password = password;
        return (V) this;
    }

    public String getEmail() {
        return this.email;
    }

    V setEmail(String email) {
        this.email = email;
        return (V) this;
    }

    public String getFullname() {
        return this.fullname;
    }

    public V setFullname(String fullname) {
        this.fullname = fullname;
        return (V) this;
    }

    public Boolean getDontSendPromo() {
        return this.dontSendPromo;
    }

    public V setDontSendPromo(Boolean promo) {
        this.dontSendPromo = promo;
        return (V) this;
    }

    Browser browser() {
        return browser;
    }

    public Page nowLookingAt() {
        return browser.page();
    }

    public Set<Cookie> closeSoftly() {
        return browser.closeSoftly();
    }

    public void closeBrowser() {
        browser.close();
    }

    public V click(WebElement webElement) {
        webElement.click();
        return (V) this;
    }

    public Visitor reset() {
        browser.page().reset();
        browser.waitForDialog();
        return this;
    }

    public Popup openAddPopup() {
        return browser.page().openAddPopup();
    }

    public Visitor forgot(String what) {
        browser.page().forgot(what);
        return this;
    }

    public V with() {
        throw new IllegalStateException("You can not call Visitor.with() method.");
    }

    public V origin() {
        if (origin == null) {
            return (V) this;
        }
        return (V) origin.origin();
    }

    @Override
    public void free() {
        V theOrigin = this.origin();
        if (this.pool == null) {
            throw new IllegalStateException("VisitorPool must be set to Visitor on adding pool.");
        }
        this.pool.free(theOrigin);
    }

    public void setPlus18(boolean plus18) {
        boolean plus181 = plus18;
    }

    public void loadElements() {
        browser.loadElements();
    }

    public void deleteCookie(String name) {
        browser.deleteCookie(name);
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getTcNo() {
        return tcNo;
    }

    public void setTcNo(String tcNo) {
        this.tcNo = tcNo;
    }
}