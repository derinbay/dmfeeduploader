package Users;

/**
 * Created by taylanderinbay on 8.11.15.
 */
interface Account<P extends Pool> {
    void setPool(P pool);

    void free();

    String getUsername();
}
